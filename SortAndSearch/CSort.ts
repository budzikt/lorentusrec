import { SortClass } from "./SortClass";


export class CSort extends SortClass {
    public sort(arrIn: number[]): number[] {
        return this.Bogosort(arrIn);
    }
    
    /* Impementation stolen form w3resource */
    private Bogosort(arr: number[]){

        let isSorted = (arr: number[]) => {
            for(var i = 1; i < arr.length; i++){
                if (arr[i-1] > arr[i]) {
                    return false;
                }
            }
            return true;
        };
    
        let shuffle = (arr: number[]) => {
            let count = arr.length, temp, index;
    
            while(count > 0){
                index = Math.floor(Math.random() * count);
                count--;
    
                temp = arr[count];
                arr[count] = arr[index];
                arr[index] = temp;
            }
    
            return arr;
        }
    
       let sort = (arr: number[]) => {
            var sorted = false;
            while(!sorted){
                arr = shuffle(arr);
                sorted = isSorted(arr);
            }
            return arr;
        }
    
        return sort(arr);
    }

}