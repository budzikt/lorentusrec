import { ASort } from './ASort';
import { BSort } from './BSort';
import { CSort } from './CSort';
import { BSearch } from './BSearch';


let unsorted = [13, 2, 17, 5, 77, 22, 83, 65, 14, 9, 0, 4, 7, 32];
const elementsToFind = [1, 5, 13, 27, 77];

let aS: ASort = new ASort();
let bS: BSort = new BSort();
let cS: CSort = new CSort();

console.log(aS.sort(unsorted.slice()));
console.log(bS.sort(unsorted.slice()));

/* Only for those, who have planty of time */
/* console.log(cS.sort(unsorted.slice())); */

/* No, due to constructor privacy */
// let binSearch: BSearch = new BSearch();
let binSearch: BSearch = BSearch.getInstance();
console.log(binSearch.binSearchEasy(elementsToFind, 27, {start: 0, stop: elementsToFind.length -1}));