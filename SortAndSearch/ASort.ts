import { SortClass } from "./SortClass";


export class ASort extends SortClass {
    
    public sort(arrIn: number[]): number[] {
        return this.insertionSort(arrIn);
    }

    private insertionSort = (inputArr: number[]) => {
        
        let length = inputArr.length;

        for (let arrIdx = 1; arrIdx < length; arrIdx++) {
            let compValue = inputArr[arrIdx];
            let j = arrIdx - 1;
            while (j >= 0 && inputArr[j] > compValue) {
                inputArr[j + 1] = inputArr[j];
                j-=1;
            }
            inputArr[j + 1] = compValue;
        }
        return inputArr;
    };
}