export abstract class SortClass {
    public abstract sort(arrIn: number[]): number[];
}
