export class BSearch {

    private static instance: BSearch;
    private initiailzed: boolean = false;
    private static opCnt: number;

    static get operationCnt() {
        return BSearch.opCnt;
    }

    private constructor() {
        this.initiailzed = true;
        BSearch.opCnt = 0;
    }

    public static getInstance(): BSearch {
        return this.instance ? this.instance : this.generateInstance();
    }

    private static generateInstance(): BSearch {
        this.instance = new BSearch();
        return this.instance;
    }

    public binSearchEasy(arr: number[], val: number, orgIdx: {start: number, stop: number }): number {
        
        let halfIdx = Math.floor((orgIdx.stop - orgIdx.start) / 2) + orgIdx.start;
        if(arr[halfIdx] === val) {
            BSearch.opCnt++;
            return halfIdx;
        }
        else if(orgIdx.stop - orgIdx.start <= 0) {
            BSearch.opCnt++;
            return -1;
        }
        else {
            if(arr[halfIdx] <= val) {
                return this.binSearchEasy(arr, val, {start: halfIdx+1, stop: orgIdx.stop});
            } else {
                return this.binSearchEasy(arr, val, {start: orgIdx.start, stop: halfIdx});
            }
        }
    }
  
}