import { SortClass } from "./SortClass";

export class BSort extends SortClass {
    
    public sort(arrIn: number[]): number[] {
        return this.bubbleSort(arrIn);
    }

    private bubbleSort(arrIn: number[]): number[] {
        let len = arrIn.length;

        for (let arrIdx = 0; arrIdx < len; arrIdx++) {
            for (let bubbleIdx = 0; bubbleIdx < (len - arrIdx); bubbleIdx++) {
                if(arrIn[bubbleIdx] > arrIn[bubbleIdx + 1]) {
                    let tmp = arrIn[bubbleIdx];
                    arrIn[bubbleIdx] = arrIn[bubbleIdx + 1];
                    arrIn[bubbleIdx + 1] = tmp;
                }
            }
            
        }
        return arrIn;
    }

}